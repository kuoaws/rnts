import { StyleSheet, Text, View, Pressable } from 'react-native'
import React, { PropsWithChildren } from 'react'
import Colors from '../costant/NGColors'

interface INGButtonProps {
    onPress: () => void;
}

const NGButton: React.FC<PropsWithChildren<INGButtonProps>> = ({ onPress, children }) => {
    return (
        <View style={styles.outerContainer}>
            <Pressable
                style={({ pressed }) => pressed ? [styles.innerContainer, styles.pressed] : styles.innerContainer}
                onPress={onPress}
                android_ripple={{ color: '#640233' }}
            >
                <Text style={styles.text}>{children}</Text>
            </Pressable>
        </View>
    )
}

export default NGButton

const styles = StyleSheet.create({
    outerContainer: {
        borderRadius: 28,
        margin: 4,
        overflow: 'hidden'
    },
    innerContainer: {
        backgroundColor: '#4e0329',
        paddingVertical: 8,
        paddingHorizontal: 16,
    },
    text: {
        color: 'white',
        textAlign: 'center'
    },
    pressed: {
        opacity: 25
    }
})