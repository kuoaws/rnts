import { StyleSheet, Text, View } from 'react-native'
import React, { PropsWithChildren } from 'react'
import Colors from '../costant/NGColors';

const NGCard: React.FC<PropsWithChildren> = ({ children }) => {
    return (
        <View style={styles.container}>
            {children}
        </View>
    )
}

export default NGCard

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 50,
        marginHorizontal: 24,
        padding: 16,
        backgroundColor: Colors.PURPLE,
        borderRadius: 8,
    },
})