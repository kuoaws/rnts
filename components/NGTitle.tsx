import { StyleSheet, Text, View } from 'react-native'
import React, { PropsWithChildren } from 'react'
import Colors from '../costant/NGColors'

const NGTitle: React.FC<PropsWithChildren> = ({ children }) => {
    return (
        <Text style={styles.title}>{children}</Text>
    )
}

export default NGTitle

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.YELLOW,
        textAlign: 'center',
        borderWidth: 2,
        borderColor: Colors.YELLOW,
        padding: 8,
        maxWidth: '80%'
    }
})