import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../costant/NGColors'

interface Props {
    roundNum: number;
    guessNum: number;
}

const GuessLog = ({ roundNum, guessNum }: Props) => {
    return (
        <View style={styles.container}>
            <Text>#{roundNum}</Text>
            <Text>computer guess : {guessNum}</Text>
        </View>
    )
}

export default GuessLog

const styles = StyleSheet.create({
    container: {
        marginTop: 16,
        borderColor: Colors.YELLOW,
        backgroundColor: Colors.PURPLE,
        borderWidth: 1,
        borderRadius: 40,
        padding: 8,
        marginVertical: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    }
})