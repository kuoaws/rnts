
import { useState } from 'react';
import { View, TextInput, Button, Modal, StyleSheet } from 'react-native';

interface ITodoInput {
    visable: boolean;
    addTask: (inputText: string) => void;
    cancel: () => void;
}

const TodoInput = ({ visable, addTask, cancel }: ITodoInput) => {

    const [inputText, setInputText] = useState('');

    const handleChangeText = (text: string) => {
        setInputText(text);
    }

    const handlePress = () => {
        addTask(inputText);
        setInputText('');
    }

    return (
        <Modal visible={visable} animationType='slide'>
            <View style={styles.inputContainer}>
                <TextInput
                    style={styles.textInput}
                    placeholder='enter task ...'
                    onChangeText={handleChangeText}
                    value={inputText} />

                <View style={styles.buttonContainer}>
                    <View style={styles.button}>
                        <Button title='Add Task' onPress={handlePress} />
                    </View>
                    <View style={styles.button}>
                        <Button title='Cancel' onPress={cancel} />
                    </View>

                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 16,
        marginBottom: 8,
        borderBottomColor: '#cccccc',
        borderBottomWidth: 1
    },
    textInput: {
        borderColor: '#cccccc',
        borderWidth: 1,
        width: '100%',
        paddingLeft: 8
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop: 16
    },
    button: {
        width: 100,
        marginHorizontal: 8
    }
});

export default TodoInput