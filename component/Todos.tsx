import { useEffect, useState } from 'react';
import { View, FlatList, Button, StyleSheet } from 'react-native'
import { Colors } from 'react-native/Libraries/NewAppScreen';

import TodoInput from './TodoInput';
import { ITodoItem, TodoItem } from './TodoItem'

const Todos = () => {

    const [modalVisable, setModalVisable] = useState(false);
    const [tasks, setTasks] = useState<ITodoItem[]>([]);

    const openModal = () => {
        setModalVisable(true)
    }

    const closeModal = () => {
        setModalVisable(false)
    }

    const addTask = (inputText: string) => {
        setTasks(prev => [...prev, { name: inputText, id: Math.random().toString() }]);
        setModalVisable(false);
    }

    const deleteTask = (id: string) => {
        setTasks(prev => prev.filter(x => x.id !== id))
    }

    useEffect(() => {
        console.log(tasks)
    }, [tasks])


    return (
        <View style={styles.container}>

            <Button title='add new task' color='#5e0acc' onPress={openModal} />
            {modalVisable && <TodoInput visable={modalVisable} addTask={addTask} cancel={closeModal} />}

            <View style={styles.taskListContainer}>
                <FlatList
                    data={tasks}
                    renderItem={({ item }) => <TodoItem {...item} delete={deleteTask} />}
                    keyExtractor={(item: ITodoItem) => item.id} />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.lighter,
        margin: 8,
        padding: 8
    },
    taskListContainer: {
        flex: 4
    },
});

export default Todos