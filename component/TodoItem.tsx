import { StyleSheet, Text, View, Pressable } from 'react-native'
import React from 'react'

export interface ITodoItem {
    id: string;
    name: string;
}

interface TodoItemProps extends ITodoItem {
    delete: (id: string) => void;
}

export const TodoItem = (props: TodoItemProps) => {

    return (
        <View style={styles.todoItem}>
            <Pressable android_ripple={{ color: '#dddddd' }} onPress={props.delete.bind(this, props.id)}>
                <Text style={styles.text}>{props.name}</Text>
            </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    todoItem: {
        margin: 8,
        borderRadius: 6,
        backgroundColor: '#5e0acc'
    },
    text: {
        color: 'white',
        padding: 8,
    }
})