import { StyleSheet, Text, View } from 'react-native'
import React, { PropsWithChildren } from 'react'
import Colors from '../costant/NGColors'

const NumberContainer: React.FC<PropsWithChildren> = ({ children }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{children}</Text>
        </View>
    )
}

export default NumberContainer

const styles = StyleSheet.create({
    container: {
        borderWidth: 4,
        borderColor: Colors.YELLOW,
        padding: 16,
        margin: 16,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        color: Colors.YELLOW,
        fontSize: 27,
        fontWeight: 'bold'
    }
})