/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useState, type PropsWithChildren } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  ImageBackground
} from 'react-native';

import {
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import LinearGradient from 'react-native-linear-gradient';

// import Todos from './component/Todos';
import StartGameScreen from './screens/StartGameScreen';
import GameOverScreen from './screens/GameOverScreen';
import GameScreen from './screens/GameScreen';
import Colors from './costant/NGColors'

const App = () => {

  const [choseNumber, setChoseNumber] = useState<number>();
  const [gameIsOver, setGameIsOver] = useState(true);
  const [rounds, setRounds] = useState(0);

  const handleChoseNumber = (number: number) => {
    setChoseNumber(number);
    setGameIsOver(false);
  }

  const handleGameOver = (gameRounds: number) => {
    setGameIsOver(true);
    setRounds(gameRounds);
  }

  const handleStartNewGame = () => {
    setChoseNumber(undefined);
    setRounds(0);
  }

  let screen = <StartGameScreen onChoseNumber={handleChoseNumber} />

  if (choseNumber) {
    screen = <GameScreen choseNum={choseNumber} onGameOver={handleGameOver} />
  }

  if (gameIsOver && choseNumber) {
    screen = <GameOverScreen rounds={rounds} choseNum={choseNumber} onRestart={handleStartNewGame} />
  }

  return (
    <LinearGradient
      colors={[Colors.PURPLE, Colors.YELLOW]}
      style={styles.rootScreen}
    >
      <ImageBackground
        source={require('./assests/background.jpg')}
        resizeMode="cover"
        style={styles.rootScreen}
        imageStyle={styles.imageBackground}
      >
        <SafeAreaView style={styles.rootScreen}>
          {screen}
        </SafeAreaView>
      </ImageBackground>
    </LinearGradient>
  )

  //   const isDarkMode = useColorScheme() === 'dark';

  //   const backgroundStyle = {
  //     backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  //   };

  //   return (
  //     // <StartGameScreen />
  //     <SafeAreaView style={{ backgroundColor: backgroundStyle.backgroundColor }}>
  //       <StatusBar
  //         barStyle={isDarkMode ? 'light-content' : 'dark-content'}
  //         backgroundColor={backgroundStyle.backgroundColor}
  //       />
  //       <StartGameScreen />
  //       {/* <Todos /> */}

  //     </SafeAreaView>
  //   );
  // };
}

const styles = StyleSheet.create({
  rootScreen: {
    flex: 1,
  },
  imageBackground: {
    opacity: 0.15
  }
})

export default App;
