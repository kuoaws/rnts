import { StyleSheet, Text, View, Alert, FlatList } from 'react-native'
import React, { useEffect, useMemo, useRef, useState } from 'react'

import NGTitle from '../components/NGTitle'
import NumberContainer from '../component/NumberContainer'
import NGButton from '../components/NGButton'
import NGCard from '../components/NGCard'
import GuessLog from '../component/GuessLog'
import AntDesign from 'react-native-vector-icons/AntDesign'

const generateRandomBetwenn = (min: number, max: number, exclude: number): number => {
    console.log(`generateRandomBetwenn, exclude: ${exclude}, min: ${min}, max: ${max}`)

    const rndNum = Math.floor(Math.random() * (max - min)) + min;
    if (rndNum === exclude) {
        return generateRandomBetwenn(min, max, exclude);
    } else {
        return rndNum;
    }
}

interface IProps {
    choseNum: number;
    onGameOver: (gameRounds: number) => void;
}

const GameScreen = ({ choseNum, onGameOver }: IProps) => {

    let minBoundry = useRef(1);
    let maxBoundry = useRef(100);

    const initialGuess = useMemo(() => generateRandomBetwenn(minBoundry.current, maxBoundry.current, choseNum), [choseNum]);
    const [currentGuess, setCurrentGuess] = useState(initialGuess);
    const [guessRound, setGuessRound] = useState([initialGuess]);

    useEffect(() => {
        if (choseNum === currentGuess) {
            console.log('game over')
            onGameOver(guessRound.length);
        }
    }, [currentGuess]);

    useEffect(() => {
        minBoundry.current = 1;
        maxBoundry.current = 100;

        console.log(`init first game, choseNum: ${choseNum}, min: ${minBoundry.current}, max: ${maxBoundry.current}`)
    }, []);

    const handleNextGuess = (direction: string) => {

        const isLie = (direction === 'lower' && currentGuess < choseNum) || (direction === 'greater' && currentGuess > choseNum)

        if (isLie) {
            Alert.alert('it is lie', 'it is wrong ...', [{ text: 'sorry', style: 'cancel' }])
            return;
        }

        if (direction === 'lower') {
            maxBoundry.current = currentGuess;
        } else {
            minBoundry.current = currentGuess + 1;
        }

        console.log(`NextGuess, choseNum: ${choseNum}, min: ${minBoundry.current}, max: ${maxBoundry.current}`)

        const newRandomNum = generateRandomBetwenn(minBoundry.current, maxBoundry.current, currentGuess);
        setCurrentGuess(newRandomNum);
        setGuessRound(pre => [newRandomNum, ...pre]);
    }

    return (
        <View style={styles.screen}>
            <NGTitle>Hello Guess</NGTitle>
            <NumberContainer>{currentGuess}</NumberContainer>
            <NGCard>
                <Text>Higher or lower ?</Text>
                <View style={styles.buttonsContainer}>
                    <NGButton onPress={handleNextGuess.bind(this, 'lower')}>
                        <AntDesign name="minus" size={24} />
                    </NGButton>
                    <NGButton onPress={handleNextGuess.bind(this, 'greater')}>
                        <AntDesign name="plus" size={24} />
                    </NGButton>
                </View>
            </NGCard>
            <View style={styles.listContainer}>
                <FlatList
                    data={guessRound}
                    renderItem={({ item, index }) => <GuessLog roundNum={index} guessNum={item} />}
                    keyExtractor={(item) => item.toString()} />
            </View>
        </View>
    )
}

export default GameScreen

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 16,
        alignItems: 'center'
    },
    buttonsContainer: {
        flexDirection: 'row'
    },
    listContainer: {
        flex: 1,
        padding: 16
    }

})