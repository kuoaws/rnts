import { StyleSheet, Text, View, TextInput, Alert } from 'react-native'
import React, { useState } from 'react'
import NGButton from '../components/NGButton'
import NGCard from '../components/NGCard';
import Colors from '../costant/NGColors';

interface IProps {
    onChoseNumber: (number: number) => void;
}

const StartGameScreen: React.FC<IProps> = ({ onChoseNumber }) => {
    const [enterNumber, setEnterNumber] = useState('');

    const handleNumberInput = (inputText: string) => {
        setEnterNumber(inputText)
    }

    const handleResetInput = () => {
        setEnterNumber('');
    }

    const handleConfirmInput = () => {
        const choseNumber = parseInt(enterNumber);
        if (isNaN(choseNumber) || choseNumber <= 0 || choseNumber > 99) {
            Alert.alert(
                'invalid number',
                'number has to be a number between 1 and 99',
                [{ text: 'Okay', style: 'destructive', onPress: handleResetInput }]
            )

            return;
        }

        onChoseNumber(choseNumber);
    }

    return (
        <NGCard>
            <TextInput
                style={styles.numberInput}
                maxLength={2}
                keyboardType='number-pad'
                value={enterNumber}
                onChangeText={handleNumberInput}
            />

            <View style={styles.groupButtonContainer}>
                <View style={styles.buttonContainer}>
                    <NGButton onPress={handleResetInput}>Reset</NGButton>
                </View>
                <View style={styles.buttonContainer}>
                    <NGButton onPress={handleConfirmInput} >Confirm</NGButton>
                </View>
            </View>
        </NGCard>
    )
}

export default StartGameScreen

const styles = StyleSheet.create({
    numberInput: {
        height: 50,
        width: 50,
        fontSize: 18,
        borderBottomColor: Colors.YELLOW,
        borderBottomWidth: 2,
        color: Colors.YELLOW,
        marginVertical: 8,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    groupButtonContainer: {
        flexDirection: 'row'
    },
    buttonContainer: {
        flex: 1
    }
})