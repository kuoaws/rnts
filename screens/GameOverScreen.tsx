import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import NGButton from '../components/NGButton'

interface Props {
    rounds: number;
    choseNum: number;
    onRestart: () => void;
}

const GameOverScreen = ({ rounds, choseNum, onRestart }: Props) => {
    return (
        <View style={styles.container}>
            <View style={styles.imageContainer}>
                <Image style={styles.image} source={require('../assests/success.png')} />
            </View>
            <Text>
                your phone need <Text>{rounds}</Text> rounds to guess the number <Text>{choseNum}</Text>
            </Text>
            <NGButton onPress={onRestart}>Start New Game</NGButton>
        </View>
    )
}

export default GameOverScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 24,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageContainer: {
        width: 300,
        height: 300,
        borderRadius: 150,
        borderWidth: 3,
        borderColor: 'black',
        overflow: 'hidden',
        margin: 36
    },
    image: {
        width: '100%',
        height: '100%'
    }
})